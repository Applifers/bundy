package ph.com.applife.bundyclock;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.MobileAds;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Date;

public class ChangePassword extends AppCompatActivity {
    public static final int CONNECTION_TIMEOUT = 30000;
    public static final int READ_TIMEOUT = 15000;

    private EditText emailadd, pass, cpass;
    String email, password, confirm, date;
    String valid_email;
    String getDate;

    public static String emailfrom;
    public static String  friendly_alias;
    public static String  emailsendto;
    private PreferenceHelper preferenceHelper;

    private static final String TAG_email = "email";
    private static final String TAG_password="password";
    private static final String TAG_date="date";




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_password);

        preferenceHelper = new PreferenceHelper(this);

        emailadd = (EditText) findViewById(R.id.etEmail);
        emailadd.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                Is_Valid_Email(emailadd); // pass your EditText Obj here.
            }

            public void Is_Valid_Email(EditText edt) {
                if (edt.getText().toString() == null) {
                    edt.setError("Invalid Email Address");
                    valid_email = null;
                } else if (isEmailValid(edt.getText().toString()) == false) {
                    edt.setError("Invalid Email Address");
                    valid_email = null;
                } else {
                    valid_email = edt.getText().toString();
                }

            }

            boolean isEmailValid(CharSequence email) {
                return android.util.Patterns.EMAIL_ADDRESS.matcher(email)
                        .matches();
            }
        });
        pass = (EditText) findViewById(R.id.etNewp);

        cpass = (EditText) findViewById(R.id.etNewpC);

        //Declaring Date (Year-Month-Date)
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd");
        getDate = (dateFormat.format(new Date()));
        dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        getDate = (dateFormat.format(new Date()));


        //Declaring Button
        Button button = (Button) findViewById(R.id.btnCancel);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Go to Dashboard Activity
                //manager.setPreferences(ChangePassword.this, "status", "0");
                Intent intent = new Intent(ChangePassword.this, Dashboard.class);
                startActivity(intent);
                ChangePassword.this.finish();
            }
        });

    }

    public void goChangePassword(View arg0) {
        //check if my internet connection
        isInternetOn();

        email = emailadd.getText().toString();
        password = pass.getText().toString();
        confirm = cpass.getText().toString();
        date = getDate;

        // Messages if empty
        if (email.equals("")) {
            //Check if empty
            completetheform();
        } else if (!(password.equals(confirm))) {
            //Check if empty
            completetheform();
        } else if (password.equals("")) {
            //Check if empty
            completetheform();
        } else if (password.length() < 8) {
            //Check if the length of password is eight characters
            minimumof8();
        } else if (confirm.equals("")) {
            //Check if empty
            completetheform();
        } else {
            // Calling the function of AsyncRegister for registering the data from the Register Form
            new AsyncChangePassword().execute(email, password, date);
        }
    }

    private class AsyncChangePassword extends AsyncTask<String, String, String> {
        ProgressDialog pdLoading = new ProgressDialog(ChangePassword.this);
        HttpURLConnection conn;
        URL url = null;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            //this method will be running on UI thread
            pdLoading.setMessage("\tLoading...");
            pdLoading.setCancelable(false);
            pdLoading.show();
        }

        @Override
        protected String doInBackground(String... params) {
            try {
                // Enter URL address where your php file resides
                url = new URL("http://applife.com.ph/bundy/changepassword.php");

            } catch (MalformedURLException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
                return "exception";
            }
            try {
                // Setup HttpURLConnection class to send and receive data from php and mysql
                conn = (HttpURLConnection)url.openConnection();
                conn.setReadTimeout(READ_TIMEOUT);
                conn.setConnectTimeout(CONNECTION_TIMEOUT);
                conn.setRequestMethod("POST");
                // setDoInput and setDoOutput method depict handling of both send and receive
                conn.setDoInput(true);
                conn.setDoOutput(true);
                // Append parameters to URL
                Uri.Builder builder = new Uri.Builder()
                        .appendQueryParameter("email", params[0])
                        .appendQueryParameter("password", params[1])
                        .appendQueryParameter("date", params[2]);
                String query = builder.build().getEncodedQuery();
                // Open connection for sending data
                OutputStream os = conn.getOutputStream();
                BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(os, "UTF-8"));
                writer.write(query);
                writer.flush();
                writer.close();
                os.close();
                conn.connect();
            } catch (IOException e1) {
                // TODO Auto-generated catch block
                e1.printStackTrace();
                return "exception";
            }
            try {
                int response_code = conn.getResponseCode();
                // Check if successful connection made
                if (response_code == HttpURLConnection.HTTP_OK) {
                    // Read data sent from server
                    InputStream input = conn.getInputStream();
                    BufferedReader reader = new BufferedReader(new InputStreamReader(input));
                    StringBuilder result = new StringBuilder();
                    String line;
                    while ((line = reader.readLine()) != null) {
                        result.append(line);
                    }
                    // Pass data to onPostExecute method
                    return(result.toString());
                }else{
                    return("unsuccessful");
                }
            } catch (IOException e) {
                e.printStackTrace();
                return "exception";
            } finally {
                conn.disconnect();
            }
        }
        @Override
        protected void onPostExecute(String result) {
            pdLoading.dismiss();

            if(result.equalsIgnoreCase("nyay!")) {
                //Calling eaddexist function
                eaddexist();
            }else {

                try {
                    JSONArray jArray = new JSONArray(result);
                    // Extract data from json and store into ArrayList as class objects
                    //
                    for (int i = 0; i > jArray.length(); i++) {
                        JSONObject c = jArray.getJSONObject(i);
                        email = c.getString(TAG_email);
                        password = c.getString(TAG_password);
                        date = c.getString(TAG_date);
                    }

                    emailfrom = email;
                    friendly_alias = password;
                    emailsendto = date;


                    //Calling function "welcome"
                    welcome();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    private void welcome() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this,R.style.AppCompatAlertDialogStyle);
        builder.setTitle(R.string.title);
        builder.setIcon( android.R.drawable.ic_dialog_info);
        builder.setMessage("Congrats,You successfully change your password.");
        builder.setCancelable(false);
        builder.setPositiveButton("OK",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog,
                                        int which) {
                        preferenceHelper.putIsLogin(false);
                        Intent intent = new Intent(ChangePassword.this,Login.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(intent);
                        ChangePassword.this.finish();
                    }
                });
        builder.show();
    }
    private void minimumof8() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this,R.style.AppCompatAlertDialogStyle);
        builder.setTitle("Alert Message:");
        builder.setIcon( android.R.drawable.stat_notify_error);
        builder.setMessage("The password should consist of 8 characters, please try again. Thank you.");
        builder.setCancelable(false);
        builder.setPositiveButton("OK", null);
        builder.show();
    }
    private void eaddexist() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this,R.style.AppCompatAlertDialogStyle);
        builder.setTitle("Alert Message:");
        builder.setIcon( android.R.drawable.stat_notify_error);
        builder.setMessage("You are about to change the Password of your account.");
        builder.setCancelable(true);
        builder.setPositiveButton("OK",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog,
                                        int which) {
                        preferenceHelper.putIsLogin(false);
                        Intent intent = new Intent(ChangePassword.this,Login.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(intent);
                        ChangePassword.this.finish();
                    }
                });
        builder.show();
    }
    private void completetheform() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this,R.style.AppCompatAlertDialogStyle);
        builder.setTitle("Alert Message:");
        builder.setIcon( android.R.drawable.stat_notify_error);
        builder.setMessage("Please complete the form to continue. Thank you.");
        builder.setCancelable(false);
        builder.setPositiveButton("OK", null);
        builder.show();
    }
    private void error() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this,R.style.AppCompatAlertDialogStyle);
        builder.setTitle("Alert Message:");
        builder.setIcon( android.R.drawable.ic_dialog_alert);
        builder.setMessage("Please check your Internet connection");
        builder.setCancelable(false);
        builder.setPositiveButton("OK",null);
        builder.show();
    }
    //Check if Internet Connection is connected
    public final boolean isInternetOn() {
        // get Connectivity Manager object to check connection
        ConnectivityManager connec =
                (ConnectivityManager)getSystemService(getBaseContext().CONNECTIVITY_SERVICE);

        // Check for network connections
        if ( connec.getNetworkInfo(0).getState() == android.net.NetworkInfo.State.CONNECTED ||
                connec.getNetworkInfo(0).getState() == android.net.NetworkInfo.State.CONNECTING ||
                connec.getNetworkInfo(1).getState() == android.net.NetworkInfo.State.CONNECTING ||
                connec.getNetworkInfo(1).getState() == android.net.NetworkInfo.State.CONNECTED ) {

            // if connected with internet
            return true;

        } else if (
                connec.getNetworkInfo(0).getState() == android.net.NetworkInfo.State.DISCONNECTED ||
                        connec.getNetworkInfo(1).getState() == android.net.NetworkInfo.State.DISCONNECTED  ) {
            error();
            return false;
        }
        return false;
    }
    @Override
    public void onBackPressed() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this,R.style.AppCompatAlertDialogStyle);
        builder.setTitle("Error!");
        builder.setIcon( android.R.drawable.ic_dialog_alert);
        builder.setMessage("Press back to Dashboard.");
        builder.setCancelable(false);
        builder.setPositiveButton("OK",null);
        builder.show();
    }
}
