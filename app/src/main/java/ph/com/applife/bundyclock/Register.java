package ph.com.applife.bundyclock;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import org.json.JSONException;

import java.io.IOException;
import java.util.HashMap;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdSize;
import com.google.android.gms.ads.AdView;

import com.google.android.gms.ads.MobileAds;


public class Register extends AppCompatActivity {
  private AdView mAdView;
  private EditText etname, ethobby, etusername, etpassword, etsendto,etempnum;
  private Button btnregister,btncancel;
  private TextView tvlogin;
  private ParseContent parseContent;
  private PreferenceHelper preferenceHelper;
  private final int RegTask = 1;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_register);

    preferenceHelper = new PreferenceHelper(this);
    parseContent = new ParseContent(this);

    if(preferenceHelper.getIsLogin()){
      Intent intent = new Intent(Register.this,Dashboard.class);
      intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
      startActivity(intent);
      this.finish();
    }

    etempnum=(EditText)findViewById(R.id.empnum);
    etname = (EditText) findViewById(R.id.fullname);
    ethobby = (EditText) findViewById(R.id.name);
    etusername = (EditText) findViewById(R.id.username);
    etpassword = (EditText) findViewById(R.id.password);
    etsendto = (EditText)findViewById(R.id.sendto);
    btnregister = (Button) findViewById(R.id.btnReg);
    btncancel=(Button)findViewById(R.id.btnCancel);
    btncancel.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        Intent cancel=new Intent(Register.this, Login.class);
        startActivity(cancel);
        Register.this.finish();
      }
    });
    btnregister.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        try {
          register();
        } catch (IOException e) {
          e.printStackTrace();
        } catch (JSONException e) {
          e.printStackTrace();
        }
      }
    });

  }

  private void register() throws IOException, JSONException {

    if (!ApplifeUtils.isNetworkAvailable(Register.this)) {
      Toast.makeText(Register.this, "Internet is required!", Toast.LENGTH_SHORT).show();
      return;
    }
    ApplifeUtils.showSimpleProgressDialog(Register.this);
    final HashMap<String, String> map = new HashMap<>();
    map.put(ApplifeConstants.Params.EMPNUM, etempnum.getText().toString());
    map.put(ApplifeConstants.Params.NAME, etname.getText().toString());
    map.put(ApplifeConstants.Params.EMAIL, ethobby.getText().toString());
    map.put(ApplifeConstants.Params.USERNAME, etusername.getText().toString());
    map.put(ApplifeConstants.Params.PASSWORD, etpassword.getText().toString());
    map.put(ApplifeConstants.Params.SENDTO, etsendto.getText().toString());

    new AsyncTask<Void, Void, String>(){
      protected String doInBackground(Void[] params) {
        String response="";
        try {
          HttpRequest req = new HttpRequest(ApplifeConstants.ServiceType.REGISTER);
          response = req.prepare(HttpRequest.Method.POST).withData(map).sendAndReadString();
        } catch (Exception e) {
          response=e.getMessage();
        }
        return response;
      }
      protected void onPostExecute(String result) {
        //do something with response
        Log.d("newwwss", result);
        onTaskCompleted(result, RegTask);
      }
    }.execute();
  }
  private void onTaskCompleted(String response,int task) {
    Log.d("responsejson", response.toString());
    ApplifeUtils.removeSimpleProgressDialog();  //will remove progress dialog
    switch (task) {
      case RegTask:

        if (parseContent.isSuccess(response)) {

          parseContent.saveInfo(response);
          Toast.makeText(Register.this, "Registered Successfully!", Toast.LENGTH_SHORT).show();
          Intent intent = new Intent(Register.this,Dashboard.class);
          intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
          startActivity(intent);
          this.finish();

        }else {
          Toast.makeText(Register.this, parseContent.getErrorMessage(response), Toast.LENGTH_SHORT).show();
        }
    }
  }
}
