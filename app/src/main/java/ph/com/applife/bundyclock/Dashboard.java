package ph.com.applife.bundyclock;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.media.MediaScannerConnection;
import android.net.ConnectivityManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.os.Looper;
import android.provider.MediaStore;
import android.util.Base64;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.InterstitialAd;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.GoogleApiClient.ConnectionCallbacks;
import com.google.android.gms.common.api.GoogleApiClient.OnConnectionFailedListener;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStates;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.android.gms.location.SettingsClient;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.navigation.NavigationView;
import com.google.firebase.remoteconfig.FirebaseRemoteConfig;
import com.google.firebase.remoteconfig.FirebaseRemoteConfigSettings;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Random;

import javax.net.ssl.HttpsURLConnection;

import static com.google.android.gms.location.LocationServices.getFusedLocationProviderClient;
public class Dashboard extends AppCompatActivity implements Updatehelper.OnUpdateNeededListener, NavigationView.OnNavigationItemSelectedListener, ConnectionCallbacks, OnConnectionFailedListener, LocationListener {

    private static String reportID;
    private InterstitialAd mInterstitialAd;

    public static final int CONNECTION_TIMEOUT = 10000;
    public static final int READ_TIMEOUT = 15000;
    private long UPDATE_INTERVAL = 10 * 1000;  /* 10 secs */
    private long FASTEST_INTERVAL = 2000; /* 2 sec */
    private static final int PERMISSION_ACCESS_COARSE_LOCATION = 99, MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE = 1, CAMERA = 2;
    Uri actualUri;
    GPSTracker gps;

    public static String combo;
    String  date, time, name, email,sendto,empnum, subject,sameID;
    String getDate, getTime;
    Bitmap bitmap;
    private static final String IMAGE_DIRECTORY = "/demonuts";
    ProgressDialog progressDialog;
    private String todate;
    private TextView dateto;
    private TextView timeto;
    private String totime;
    private TextView reportno;
    private boolean haveimage,timein;
    private TextView tvname;
    private TextView tvhobby;
    private TextView tvcode;
    private TextView tvemail;
    private TextView tvnumber, output,timeoutt,timeint;
    private EditText etsubject;
    private String gpslongitude,lat = "null",longi = "null",exact;
    private String gpslatitude;
    private Button btnlogout, btntimein, btntimeout, btnchangeEmail, btnchangePassword;
    private PreferenceHelper preferenceHelper;
    private final int RegTask = 1;
    private final int LoginTask = 1;
    private LocationRequest mLocationRequest = new LocationRequest();
    private ParseContent parseContent;
    GoogleApiClient mGoogleApiClient;
    static Location mLastLocation;
    String HttpUrl1, text;

    private FirebaseRemoteConfig firebaseRemoteConfig = FirebaseRemoteConfig.getInstance();
    private HashMap<String,Object> firebaseDefault;
    private final String LATEST_APP_VERSION_KEY = "latest_app_version";
    InterstitialAd adview2;
    private FloatingActionButton fabin,fabout;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_dashboard);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        startLocationUpdates();
        parseContent = new ParseContent(this);
        adview2 = new InterstitialAd(Dashboard.this);
        adview2.setAdUnitId(getString(R.string.inter));
        // mAdView = (AdView) findViewById(R.id.adView);
        AdRequest adRequest = new AdRequest.Builder().build();

        adview2.loadAd(adRequest);
        adview2.setAdListener(new AdListener(){
            public void onAdLoaded(){
                displayInterstitial();
            }
        });
        firebaseDefault = new HashMap<>();
        firebaseDefault.put(LATEST_APP_VERSION_KEY,getCurrentVersionCode());
        firebaseRemoteConfig.setDefaults(firebaseDefault);
        firebaseRemoteConfig.setConfigSettings(new FirebaseRemoteConfigSettings.Builder().setDeveloperModeEnabled(true).build());
        firebaseRemoteConfig.fetch().addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete( Task<Void> task) {

                if (task.isSuccessful()) {
                    firebaseRemoteConfig.activateFetched();
                    checkForUpdate();
                } else {
                    Toast.makeText(Dashboard.this, "Something went wrong fetching", Toast.LENGTH_SHORT).show();
                }
            }
        });


        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        NavigationView navigationView = findViewById(R.id.nav_view);
        View headerView = navigationView.getHeaderView(0);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();
        navigationView.setNavigationItemSelectedListener(this);

        preferenceHelper = new PreferenceHelper(this);
        reportno            =(TextView)findViewById(R.id.txtreportID);
        timeto              =(TextView) findViewById(R.id.Times);
        dateto              =(TextView)findViewById(R.id.Dates);
        tvnumber            =(TextView)findViewById(R.id.textView3);
        tvemail             =(TextView)findViewById(R.id.textView2);
        tvhobby             =(TextView) findViewById(R.id.tvhobby);
        tvname              =(TextView) findViewById(R.id.tvname);
        timeint             =(TextView) findViewById(R.id.timeint);
        timeoutt              =(TextView) findViewById(R.id.timeoutt);
        //btnlogout           =(Button) findViewById(R.id.btn);
        fabin = (FloatingActionButton) this.findViewById(R.id.timeinn);
        fabout = (FloatingActionButton) this.findViewById(R.id.timeouttt);
        etsubject           =(EditText) findViewById(R.id.editText2);
        btnchangePassword   =(Button)findViewById(R.id.imageButton3);
        btnchangeEmail      =(Button)findViewById(R.id.imageButton4);
       TextView text1 = (TextView)headerView.findViewById(R.id.name);
       TextView text2 = (TextView)headerView.findViewById(R.id.email);
        TextView text3 = (TextView)headerView.findViewById(R.id.emp);
        TextView text4 = (TextView)headerView.findViewById(R.id.send);
        requestStoragePermission();
        tvnumber.setText(preferenceHelper.getNum());
        tvname.setText(preferenceHelper.getName());
        tvhobby.setText(preferenceHelper.getSENDTO());
        tvemail.setText(preferenceHelper.getEm());
        text1.setText(preferenceHelper.getName());
        text2.setText(preferenceHelper.getEm());
        timeint.setText(preferenceHelper.getTime());
        timeoutt.setText(preferenceHelper.getTimeout());
        text3.setText("Employee No: "+preferenceHelper.getNum());
        text4.setText("Send to:" +preferenceHelper.getSENDTO());
        output = (TextView)findViewById(R.id.txtwelcome);
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();

        if (mGoogleApiClient != null) {
            mGoogleApiClient.connect();
        } else
            Toast.makeText(this, "Not Connected!", Toast.LENGTH_SHORT).show();




//        btnchangePassword.setOnClickListener(new View.OnClickListener() {
//
//            @Override
//            public void onClick(View v) {
//
//                Intent cpassword=new Intent(Dashboard.this, ChangePassword.class);
//                startActivity(cpassword);
//                Dashboard.this.finish();
//            }
//        });




        fabin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                settingRequest();
                if (preferenceHelper.getdisable()){
                    Toast.makeText(getApplicationContext(), "You must time out first before you Time in", Toast.LENGTH_SHORT).show();
                    return;
                }
                if (etsubject.getText().toString().equals("")){
                    Toast.makeText(getApplicationContext(), "Please input subject", Toast.LENGTH_SHORT).show();
                    etsubject.setError("Please input subject");
                    etsubject.requestFocus();
                    return;
                }

                if (!ApplifeUtils.isNetworkAvailable(Dashboard.this)) {
                    Toast.makeText(Dashboard.this, "Internet is required!", Toast.LENGTH_SHORT).show();
                    return;
                }



                else{
                    startLocationUpdates();
                    time();
                    name = tvname.getText().toString();
                    empnum=tvnumber.getText().toString();
                    email = tvemail.getText().toString();
                    date = getDate.toString();
                    time = getTime.toString();
                    gpslatitude = lat;
                    gpslongitude = longi;
                    sendto=tvhobby.getText().toString();
                    subject=etsubject.getText().toString();
                    reportID =sameID;
                    timein = true;
                    requestStoragePermission();
                    takePhotoFromCamera();
                    HttpUrl1 = "http://applife.com.ph/bundy/timeinselfie.php";

                }


            }
        });
        fabout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                settingRequest();
                if (!preferenceHelper.getdisable()){
                    Toast.makeText(getApplicationContext(), "You must time in first before you Time out", Toast.LENGTH_SHORT).show();
                    return;
                }
                if (etsubject.getText().toString().equals("")){
                    Toast.makeText(getApplicationContext(), "Please input subject", Toast.LENGTH_SHORT).show();
                    etsubject.setError("Please input subject");
                    etsubject.requestFocus();
                    return;
                }

                if (!ApplifeUtils.isNetworkAvailable(Dashboard.this)) {
                    Toast.makeText(Dashboard.this, "Internet is required!", Toast.LENGTH_SHORT).show();
                    return;
                }


                else{
                    startLocationUpdates();
                    time();
                    name = tvname.getText().toString();
                    empnum=tvnumber.getText().toString();
                    email = tvemail.getText().toString();
                    date = getDate.toString();
                    time = getTime.toString();
                    gpslatitude = lat;
                    gpslongitude = longi;
                    sendto=tvhobby.getText().toString();
                    subject=etsubject.getText().toString();
                    reportID= sameID;
                    // new Dashboard.AsyncReports().execute(name,empnum, email, date, time, gpslatitude, gpslongitude,sendto,subject, reportID);
                    timein = false;
                    requestStoragePermission();
                    takePhotoFromCamera();
                    HttpUrl1 = "http://applife.com.ph/bundy/timeoutSelfie.php";

                }
            }
        });
        //End Change Password
        //ChangeEmail
        //End ChangeEmail

        //first random
        char[] chars1 = "ABCDEFGHIJKLMNOPQRSTUVWXYZ".toCharArray();
        StringBuilder sb1 = new StringBuilder();
        Random random1 = new Random();
        for (int i = 0; i < 3; i++) {
            char c1 = chars1[random1.nextInt(chars1.length)];
            sb1.append(c1);
        }
        String random_string = sb1.toString();

        //second random
        char[] number = "1234567890".toCharArray();
        StringBuilder sb2 = new StringBuilder();
        Random random2 = new Random();
        for (int i = 0; i < 4; i++) {
            char c2 = number[random2.nextInt(number.length)];
            sb2.append(c2);
        }
        String random_string1 = sb2.toString();
        //third random
        Calendar c = Calendar.getInstance();
        SimpleDateFormat df = new SimpleDateFormat("MMyyyydd");
        String formattedDate = df.format(c.getTime());

        combo = random_string + "-" + random_string1 + "-" + formattedDate;
        sameID=combo;
        //Checking the Permission Access
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[] { Manifest.permission.ACCESS_FINE_LOCATION },
                    PERMISSION_ACCESS_COARSE_LOCATION);
        }
        tvcode = (TextView) findViewById(R.id.txtcode);
        tvcode.setText(combo);

        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd");
        getDate = (dateFormat.format(new Date()));
        dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        getDate = (dateFormat.format(new Date()));

        SimpleDateFormat dateFormat1=new SimpleDateFormat("MM/dd/yyyy");
        todate=(dateFormat1.format(new Date()));
        dateto.setText(todate);


        //Declaring Date (Hour-Minute)
        Thread t = new Thread(){
            @Override
            public void run() {
                try {
                    while (!isInterrupted()) {
                        Thread.sleep(1000);
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                               long date = System.currentTimeMillis();
                               SimpleDateFormat sdf = new SimpleDateFormat("hh:mm:ss a");
                               String datestring  = sdf.format(date);
                                timeto.setText(datestring);

                            }
                        });
                    }
                }catch (InterruptedException e){

                }
            }
        };
        t.start();




    }

    private void displayInterstitial(){
        if (adview2.isLoaded()){
            adview2.show();
        }
        else{
            // Toast.makeText(this, "not show", Toast.LENGTH_SHORT).show();
        }
    }


    private void startLocationUpdates() {
        mLocationRequest = new LocationRequest();
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        mLocationRequest.setInterval(UPDATE_INTERVAL);
        mLocationRequest.setFastestInterval(FASTEST_INTERVAL);

        // Create LocationSettingsRequest object using location request
        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder();
        builder.addLocationRequest(mLocationRequest);
        LocationSettingsRequest locationSettingsRequest = builder.build();

        // Check whether location settings are satisfied
        // https://developers.google.com/android/reference/com/google/android/gms/location/SettingsClient
        SettingsClient settingsClient = LocationServices.getSettingsClient(this);
        settingsClient.checkLocationSettings(locationSettingsRequest);

        // new Google API SDK v11 uses getFusedLocationProviderClient(this)
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        getFusedLocationProviderClient(this).requestLocationUpdates(mLocationRequest, new LocationCallback() {
                    @Override
                    public void onLocationResult(LocationResult locationResult) {
                        // do work here
                        onLocationChanged(locationResult.getLastLocation());
                    }
                },
                Looper.myLooper());


    }



    private void checkForUpdate() {
        int fetchedVersionCode = (int) firebaseRemoteConfig.getDouble(LATEST_APP_VERSION_KEY);
        if(getCurrentVersionCode() < fetchedVersionCode) {


            AlertDialog dialog = new AlertDialog.Builder(this)
                    .setTitle("New version available")
                    .setMessage("Please, update app to new version")
                    .setPositiveButton("Update",
                            new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    redirectStore("https://play.google.com/store/apps/details?id=ph.com.applife.bundyclock");
                                }
                            }).setNegativeButton("Later",
                            new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {

                                }
                            }).create();
            dialog.show();


        }
        else {
           Toast.makeText(this, "your app is already updated", Toast.LENGTH_SHORT).show();
        }
    }

    private int getCurrentVersionCode() {
        try {
            return getPackageManager().getPackageInfo(getPackageName(),0).versionCode;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        return  -1;
    }

    private void redirectStore(String updateUrl) {
        final Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(updateUrl));
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
    }
    private void ImageUploadToServerFunction() {

        if (!ApplifeUtils.isNetworkAvailable(Dashboard.this)) {
            Toast.makeText(Dashboard.this, "Internet is required!", Toast.LENGTH_SHORT).show();
            return;
        }
        ByteArrayOutputStream byteArrayOutputStreamObject;
        byteArrayOutputStreamObject = new ByteArrayOutputStream();
        //rotated = Bitmap.createScaledBitmap(rotated, 600, 600, true);
        bitmap.compress(Bitmap.CompressFormat.JPEG, 80, byteArrayOutputStreamObject);
        byte[] byteArrayVar = byteArrayOutputStreamObject.toByteArray();
        final String ConvertImage = Base64.encodeToString(byteArrayVar, Base64.DEFAULT);

        class AsyncTaskUploadClass extends AsyncTask<Void, Void, String> {
            protected void onPreExecute() {

                super.onPreExecute();

                progressDialog = ProgressDialog.show(Dashboard.this, "Image is Uploading", "Please Wait", false, false);
            }


            protected void onPostExecute(String string1) {

                super.onPostExecute(string1);

                Log.d("newwwss", string1);
                onTaskCompleted(string1,1);


            }
            private void onTaskCompleted(String response,int task) {
                Log.d("responsejson", response.toString());
                progressDialog.dismiss();  //will remove progress dialog
                switch (task) {
                    case 1:
                        if (parseContent.isSuccess(response)) {
                            if(timein){
                                //success();
                                Toast.makeText(getApplicationContext(), "Time in Successful", Toast.LENGTH_SHORT).show();
                                preferenceHelper.setdisable(true);
                                timeint.setVisibility(View.VISIBLE);
                                timeint.setText("Last timed in: \n "+getTime.toString());
                                timeoutt.setText("");
                                timeoutt.setVisibility(View.GONE);
                                preferenceHelper.setTime(timeint.getText().toString());
                                preferenceHelper.setTimeout(timeoutt.getText().toString());
                    }
                    else {
                        //tsuccess();
                                Toast.makeText(getApplicationContext(), "Time out Successful", Toast.LENGTH_SHORT).show();
                                preferenceHelper.setdisable(false);
                                etsubject.setText("");
                                timeoutt.setVisibility(View.VISIBLE);
                                timeoutt.setText("Last timed out: \n " +getTime.toString());
                                timeint.setText("");
                                timeint.setVisibility(View.GONE);
                                preferenceHelper.setTime(timeint.getText().toString());
                                preferenceHelper.setTimeout(timeoutt.getText().toString());
                    }

                        }else {
                            Toast.makeText(Dashboard.this, parseContent.getErrorMessage(response), Toast.LENGTH_SHORT).show();
                        }
                }
            }


            @Override
            protected String doInBackground(Void... params) {
                ImageProcessClass imageProcessClass = new ImageProcessClass();
                HashMap<String, String> HashMapParams = new HashMap<String, String>();
                HashMapParams.put("name", tvname.getText().toString());
                HashMapParams.put("empnum", tvnumber.getText().toString());
                HashMapParams.put("email", tvemail.getText().toString());
                HashMapParams.put("date", getDate.toString());
                HashMapParams.put("time", getTime.toString());
                HashMapParams.put("gpslatitude", lat);
                HashMapParams.put("gpslongitude", longi);
                HashMapParams.put("sendto", tvhobby.getText().toString());
                HashMapParams.put("def", getSaltString());
                HashMapParams.put("subject", etsubject.getText().toString());
                HashMapParams.put("image_path", ConvertImage);
                HashMapParams.put("reportID", sameID);
                String FinalData = imageProcessClass.ImageHttpRequest(HttpUrl1, HashMapParams);

                return FinalData;

            }


        }
        AsyncTaskUploadClass AsyncTaskUploadClassOBJ = new AsyncTaskUploadClass();
        AsyncTaskUploadClassOBJ.execute();



    }



//timeout
    private void tsuccess(){
        AlertDialog.Builder builder = new AlertDialog.Builder(this,R.style.AppCompatAlertDialogStyle);
        builder.setTitle(R.string.title);
        builder.setIcon( android.R.drawable.ic_menu_save);
        builder.setMessage(R.string.messageoftsuccess);
        builder.setCancelable(false);
        builder.setPositiveButton("OK",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog,
                                        int which) {
                        //btntimein.setEnabled(true);
                       //btntimeout.setEnabled(false);
                        preferenceHelper.setdisable(false);
                        etsubject.setText("");

                    }
                });
        builder.show();

    }

    //timein
    private void success() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this,R.style.AppCompatAlertDialogStyle);
        builder.setTitle(R.string.title);
        builder.setIcon( android.R.drawable.ic_menu_save);
        builder.setMessage(R.string.messageofsucess);
        builder.setCancelable(false);
        builder.setPositiveButton("OK",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog,
                                        int which) {
                        //btntimeout.setEnabled(true);
                        //btntimein.setEnabled(false);
                        preferenceHelper.setdisable(true);
                    }
                });
        builder.show();
    }
    private void somethingwentwrong() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this,R.style.AppCompatAlertDialogStyle);
        builder.setTitle(R.string.title);
        builder.setIcon( android.R.drawable.stat_notify_error);
        builder.setMessage("Something went wrong. Please try again.");
        builder.setCancelable(false);
        builder.setPositiveButton("OK",null);
        builder.show();
    }
    private void connectingtoserver() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this,R.style.AppCompatAlertDialogStyle);
        builder.setTitle("Error!");
        builder.setIcon( android.R.drawable.ic_dialog_alert);
        builder.setMessage("Please Try Again !");
        builder.setCancelable(false);
        builder.setPositiveButton("OK",null);
        builder.show();
    }

    public final boolean isInterneton() {
        ConnectivityManager connec =
                (ConnectivityManager)getSystemService(getBaseContext().CONNECTIVITY_SERVICE);

        if ( (connec != null ? connec.getNetworkInfo(0).getState() : null) == android.net.NetworkInfo.State.CONNECTED ||
                connec.getNetworkInfo(0).getState() == android.net.NetworkInfo.State.CONNECTING ||
                connec.getNetworkInfo(1).getState() == android.net.NetworkInfo.State.CONNECTING ||
                connec.getNetworkInfo(1).getState() == android.net.NetworkInfo.State.CONNECTED ) {
            return true;
        }
        else if (
                connec.getNetworkInfo(0).getState() == android.net.NetworkInfo.State.DISCONNECTED ||
                        connec.getNetworkInfo(1).getState() == android.net.NetworkInfo.State.DISCONNECTED  ) {
            error();
            return false;
        }
        return false;
    }

    private void error() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this,R.style.AppCompatAlertDialogStyle);
        builder.setTitle("Error!");
        builder.setIcon( android.R.drawable.ic_dialog_alert);
        builder.setMessage(R.string.messageoferror);
        builder.setCancelable(false);
        builder.setPositiveButton("OK",null);
        builder.show();
    }

    private void takePhotoFromCamera() {
            Intent intent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);

            startActivityForResult(intent, CAMERA);
    }

    public void onActivityResult(int requestCode, int resultCode, Intent resultData) {

        super.onActivityResult(requestCode, resultCode, resultData);


        if (requestCode == MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE && resultCode == Activity.RESULT_OK) {

            // The document selected by the user won't be returned in the intent.
            // Instead, a URI to that document will be contained in the return intent
            // provided to this method as a parameter.  Pull that uri using "resultData.getData()"

            if (resultData != null) {

                actualUri = resultData.getData();

                try {
                    bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), actualUri);
                    //chosen.setImageBitmap(bitmap);
                    //chosen.invalidate();
                    //rotatebutton.setEnabled(true);
                    haveimage = true;
                } catch (IOException e) {
                    e.printStackTrace();
                }

            }
            else{
                haveimage = false;
            }
        }

        if (requestCode == CAMERA && resultCode == RESULT_OK) {

            haveimage = true;
            bitmap = (Bitmap) resultData.getExtras().get("data");
            //chosen.setImageBitmap(bitmap);
            //saveImage(bitmap);
            //Toast.makeText(Dashboard.this, "Image Saved!", Toast.LENGTH_SHORT).show();
            // dealTakePhoto();
            //new Dashboard().execute(name,empnum, email, date, time, gpslatitude, gpslongitude,sendto,subject, reportID);
            //setCapturedImage(getImagePath());
            if (haveimage == true) {
                ImageUploadToServerFunction();
            }

        }
        else {
            //Toast.makeText(Dashboard.this, "canceled", Toast.LENGTH_SHORT).show();
        }


    }

    public String saveImage(Bitmap myBitmap) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        myBitmap.compress(Bitmap.CompressFormat.JPEG, 90, bytes);
        File wallpaperDirectory = new File(
                Environment.getExternalStorageDirectory() + IMAGE_DIRECTORY);
        // have the object build the directory structure, if needed.
        if (!wallpaperDirectory.exists()) {
            wallpaperDirectory.mkdirs();
        }

        try {
            File f = new File(wallpaperDirectory, Calendar.getInstance()
                    .getTimeInMillis() + ".jpg");
            f.createNewFile();
            FileOutputStream fo = new FileOutputStream(f);
            fo.write(bytes.toByteArray());
            MediaScannerConnection.scanFile(this,
                    new String[]{f.getPath()},
                    new String[]{"image/jpeg"}, null);
            fo.close();
            Log.d("TAG", "File Saved::---&gt;" + f.getAbsolutePath());

            return f.getAbsolutePath();
        } catch (IOException e1) {
            e1.printStackTrace();
        }
        return "";
    }
    private void requestStoragePermission() {

        if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.CAMERA) && ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) && ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.READ_EXTERNAL_STORAGE)  && ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.ACCESS_FINE_LOCATION)) {
            //If the user has denied the permission previously your code will come to this block
            //Here you can explain why you need this permission
            //Explain here why you need this permission
        }

        ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE,Manifest.permission.ACCESS_FINE_LOCATION,}, 6666);

    }

    public void onUpdateNeeded(final String updateUrl) {
        AlertDialog dialog = new AlertDialog.Builder(this)
                .setTitle("New version available")
                .setMessage("Please, update app to new version")
                .setCancelable(false)
                .setPositiveButton("Update",
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                redirectStore(updateUrl);
                            }
                            }
                        ).create();
        dialog.show();
    }

    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_email) {
            Intent cemail=new Intent(Dashboard.this, ChangeEmail.class);
            startActivity(cemail);
            Dashboard.this.finish();
        } else if (id == R.id.nav_pass) {
            Intent cpassword=new Intent(Dashboard.this, ChangePassword.class);
            startActivity(cpassword);
            Dashboard.this.finish();
        } else if (id == R.id.nav_logout) {
            preferenceHelper.putIsLogin(false);
                Intent intent = new Intent(Dashboard.this, MainActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
                Dashboard.this.finish();

        }

        else if (id == R.id.update) {

        firebaseDefault = new HashMap<>();
        firebaseDefault.put(LATEST_APP_VERSION_KEY,getCurrentVersionCode());
        firebaseRemoteConfig.setDefaults(firebaseDefault);
        firebaseRemoteConfig.setConfigSettings(new FirebaseRemoteConfigSettings.Builder().setDeveloperModeEnabled(true).build());
        firebaseRemoteConfig.fetch().addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete( Task<Void> task) {

                if (task.isSuccessful()) {
                    firebaseRemoteConfig.activateFetched();
                    checkForUpdate();
                } else {
                    Toast.makeText(Dashboard.this, "Something went wrong fetching", Toast.LENGTH_SHORT).show();
                }
            }
        });

        }

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    public void onConnected(Bundle bundle) {
        settingRequest();
    }

    @SuppressLint("RestrictedApi")
    private void settingRequest() {
        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(10000);    // 10 seconds, in milliseconds
        mLocationRequest.setFastestInterval(1000);   // 1 second, in milliseconds
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);

        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder()
                .addLocationRequest(mLocationRequest);

        PendingResult<LocationSettingsResult> result =
                LocationServices.SettingsApi.checkLocationSettings(mGoogleApiClient,
                        builder.build());

        result.setResultCallback(new ResultCallback<LocationSettingsResult>() {

            @Override
            public void onResult(LocationSettingsResult result) {
                final Status status = result.getStatus();
                final LocationSettingsStates state = result.getLocationSettingsStates();
                switch (status.getStatusCode()) {
                    case LocationSettingsStatusCodes.SUCCESS:
                        // All location settings are satisfied. The client can
                        // initialize location requests here.
                        getLocation();
                        break;
                    case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                        // Location settings are not satisfied, but this can be fixed
                        // by showing the user a dialog.
                        try {
                            // Show the dialog by calling startResolutionForResult(),
                            // and check the result in onActivityResult().
                            status.startResolutionForResult(Dashboard.this, 1000);
                        } catch (IntentSender.SendIntentException e) {
                            // Ignore the error.
                        }
                        break;
                    case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                        // Location settings are not satisfied. However, we have no way
                        // to fix the settings so we won't show the dialog.

                        break;
                }
            }

        });
    }

    public void getLocation() {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling

            return;
        } else {
            /*Getting the location after aquiring location service*/
            mLastLocation = LocationServices.FusedLocationApi.getLastLocation(
                    mGoogleApiClient);

            if (mLastLocation != null) {

            } else {

                Log.i("Current Location", "No data for location found");

                if (!mGoogleApiClient.isConnected())
                    mGoogleApiClient.connect();

                LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);
            }
        }
    }

    @Override
    public void onConnectionSuspended(int i) {

    }
    protected void onStop() {
        mGoogleApiClient.disconnect();
        super.onStop();
    }

    protected void onResume() {


        super.onResume();
    }



    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
        Toast.makeText(this, "Connection Failed!", Toast.LENGTH_SHORT).show();
        if (connectionResult.hasResolution()) {
            try {
                // Start an Activity that tries to resolve the error
                connectionResult.startResolutionForResult(this, 90000);
            } catch (IntentSender.SendIntentException e) {
                e.printStackTrace();
            }
        } else {
            Log.i("Current Location", "Location services connection failed with code " + connectionResult.getErrorCode());
        }
    }

    @Override
    public void onLocationChanged(Location location) {
        lat = Double.toString(location.getLatitude());
        longi= Double.toString(location.getLongitude());
        //Toast.makeText(getApplicationContext(), lat + " , " +longi,Toast.LENGTH_SHORT).show();
        //Toast.makeText(this, mss, Toast.LENGTH_SHORT).show();
        // You can now create a LatLng Object for use with maps


        Geocoder geocoder;

        geocoder = new Geocoder(this, Locale.getDefault());

        List<Address> addresses  = null;
        try {
            addresses = geocoder.getFromLocation(location.getLatitude(), location.getLongitude(), 1);
            if (addresses.size() > 0) {
                exact = addresses.get(0).getAddressLine(0);
                //Toast.makeText(getApplicationContext(), lat, Toast.LENGTH_SHORT).show();
            }
        } catch (IOException e) {
            e.printStackTrace();
            Log.e("tag", e.getMessage());
        }

    }

    private class ImageProcessClass {
        public String ImageHttpRequest(String requestURL, HashMap<String, String> PData) {

            StringBuilder stringBuilder = new StringBuilder();

            try {

                URL url;
                HttpURLConnection httpURLConnectionObject;
                OutputStream OutPutStream;
                BufferedWriter bufferedWriterObject;
                BufferedReader bufferedReaderObject;
                int RC;

                url = new URL(requestURL);

                httpURLConnectionObject = (HttpURLConnection) url.openConnection();
                httpURLConnectionObject.setReadTimeout(19000);
                httpURLConnectionObject.setConnectTimeout(19000);
                httpURLConnectionObject.setRequestMethod("POST");
                httpURLConnectionObject.setDoInput(true);

                httpURLConnectionObject.setDoOutput(true);

                OutPutStream = httpURLConnectionObject.getOutputStream();

                bufferedWriterObject = new BufferedWriter(

                        new OutputStreamWriter(OutPutStream, "UTF-8"));

                bufferedWriterObject.write(bufferedWriterDataFN(PData));

                bufferedWriterObject.flush();

                bufferedWriterObject.close();

                OutPutStream.close();

                RC = httpURLConnectionObject.getResponseCode();

                if (RC == HttpsURLConnection.HTTP_OK) {

                    bufferedReaderObject = new BufferedReader(new InputStreamReader(httpURLConnectionObject.getInputStream()));

                    stringBuilder = new StringBuilder();

                    String RC2;

                    while ((RC2 = bufferedReaderObject.readLine()) != null) {

                        stringBuilder.append(RC2);
                    }
                }

            } catch (Exception e) {
                e.printStackTrace();
            }
            return stringBuilder.toString();
        }

        private String bufferedWriterDataFN(HashMap<String, String> HashMapParams) throws UnsupportedEncodingException {

            StringBuilder stringBuilderObject;

            stringBuilderObject = new StringBuilder();

            for (Map.Entry<String, String> KEY : HashMapParams.entrySet()) {


                    stringBuilderObject.append("&");

                stringBuilderObject.append(URLEncoder.encode(KEY.getKey(), "UTF-8"));

                stringBuilderObject.append("=");

                stringBuilderObject.append(URLEncoder.encode(KEY.getValue(), "UTF-8"));
            }

            return stringBuilderObject.toString();
        }

    }

    protected String getSaltString() {
        String SALTCHARS = "ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";
        StringBuilder salt = new StringBuilder();
        Random rnd = new Random();
        while (salt.length() < 10) { // length of the random string.
            int index = (int) (rnd.nextFloat() * SALTCHARS.length());
            salt.append(SALTCHARS.charAt(index));
        }
        String saltStr = salt.toString();
        return saltStr;

    }
    public void time(){
        Date d = new Date();
        SimpleDateFormat sdf = new SimpleDateFormat("hh:mm a");
        String currentDateTimeString = sdf.format(d);
        getTime = (currentDateTimeString);

        Date ss= new Date();
        SimpleDateFormat tsk= new SimpleDateFormat("hh:mm a");
        String currentDateTimeStrings= tsk.format(ss);
        totime=(currentDateTimeStrings);
        timeto.setText(totime);
    }

    public void onBackPressed() {
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }



}
