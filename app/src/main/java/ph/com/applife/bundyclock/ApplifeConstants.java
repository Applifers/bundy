package ph.com.applife.bundyclock;

public class ApplifeConstants {

    public class ServiceType {
        public static final String BASE_URL = "http://applife.com.ph/bundy/";
        public static final String LOGIN = BASE_URL + "login.php";
        public static final String REGISTER =  BASE_URL + "register.php";
        public static final String TIMEIN =  BASE_URL + "timeMein.php";
    }
    public class Params {

        public static final String NAME = "fullname";
        public static final String EMAIL = "email";
        public static final String USERNAME = "username";
        public static final String PASSWORD = "password";
        public static final String SENDTO = "sendto";
        public static final String EMPNUM="empnum";
        public static final String DATE = "date";
        public static final String TIME = "time";
        public static final String LONGITUDE = "gpslongitude";
        public static final String LATITUDE = "gpslatitude";
        public static final String REPORTID = "reportID";

    }
}
