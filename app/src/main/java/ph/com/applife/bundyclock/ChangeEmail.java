package ph.com.applife.bundyclock;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.MobileAds;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Date;

public class ChangeEmail extends AppCompatActivity {
    private AdView mAdView;

    public static final int CONNECTION_TIMEOUT = 30000;
    public static final int READ_TIMEOUT = 15000;


    private EditText etemail, etsendto;
    Button btncancel;

    String email, sendto,date;
    String valid_email;
    String getDate;
   // String validemail;

    public static String emailfrom;
    public static String emailsendto;
    public static String dateupdate;
    private PreferenceHelper preferenceHelper;

    private static final String TAG_email = "email";
    private static final String TAG_sendto = "sendto";
    private static final String TAG_date = "date";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_email);

        preferenceHelper = new PreferenceHelper(this);

        etemail = (EditText) findViewById(R.id.etEmail);
        etemail.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                Is_Valid_Email(etemail); // pass your EditText Obj here.
            }

            public void Is_Valid_Email(EditText edt) {
                if (edt.getText().toString() == null) {
                    edt.setError("Invalid Email Address");
                    valid_email = null;
                } else if (isEmailValid(edt.getText().toString()) == false) {
                    edt.setError("Invalid Email Address");
                    valid_email = null;
                } else {
                    valid_email = edt.getText().toString();
                }

            }

            boolean isEmailValid(CharSequence email) {
                return android.util.Patterns.EMAIL_ADDRESS.matcher(email)
                        .matches();
            }
        });

        etsendto = (EditText) findViewById(R.id.etSendto);
        etsendto.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                Is_Valid_Email(etsendto); // pass your EditText Obj here.
            }

            public void Is_Valid_Email(EditText edt) {
                if (edt.getText().toString() == null) {
                    edt.setError("Invalid Email Address");
                    valid_email = null;
                } else if (isEmailValid(edt.getText().toString()) == false) {
                    edt.setError("Invalid Email Address");
                    valid_email = null;
                } else {
                    valid_email = edt.getText().toString();
                }

            }

            boolean isEmailValid(CharSequence email) {
                return android.util.Patterns.EMAIL_ADDRESS.matcher(email)
                        .matches();
            }
        });

        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd");
        getDate = (dateFormat.format(new Date()));
        dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        getDate = (dateFormat.format(new Date()));


        btncancel = (Button) findViewById(R.id.btnCancel);
        btncancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ChangeEmail.this, Dashboard.class);
                startActivity(intent);
                ChangeEmail.this.finish();
            }
        });
    }

    //Start of Change Email process

    public void goChange(View arg0) {

        isInternetOn();

        email = etemail.getText().toString();
        sendto = etsendto.getText().toString();
        date = getDate;
        if (email.equals("")) {
            completeform();
        } else if (sendto.equals("")) {
            completeform();
        } else {
           new AsyncChangeEmail().execute(email, sendto, date);
        }
    }
    private class AsyncChangeEmail extends AsyncTask<String, String, String> {
        ProgressDialog pdLoading = new ProgressDialog(ChangeEmail.this);
        HttpURLConnection conn;
        URL url = null;


        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            pdLoading.setMessage("\tLoading...");
            pdLoading.setCancelable(false);
            pdLoading.show();
        }

        @Override
        protected String doInBackground(String... params) {
            try {
                url = new URL("http://applife.com.ph/bundy/changeemail.php");
            } catch (MalformedURLException e) {
                e.printStackTrace();
                return "exception";
            }
            try {
                conn = (HttpURLConnection) url.openConnection();
                conn.setReadTimeout(READ_TIMEOUT);
                conn.setConnectTimeout(CONNECTION_TIMEOUT);
                conn.setRequestMethod("POST");

                conn.setDoInput(true);
                conn.setDoOutput(true);

                Uri.Builder builder = new Uri.Builder()
                        .appendQueryParameter("email", params[0])
                        .appendQueryParameter("sendto", params[1])
                        .appendQueryParameter("date", params[2]);
                String query = builder.build().getEncodedQuery();

                OutputStream os = conn.getOutputStream();
                BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(os, "UTF-8"));
                writer.write(query);
                writer.flush();
                writer.close();
                os.close();
                conn.connect();
            } catch (IOException el) {
                el.printStackTrace();
                return "exception";
            }
            try {
                int response_code = conn.getResponseCode();

                if (response_code == HttpURLConnection.HTTP_OK) {
                    InputStream input = conn.getInputStream();
                    BufferedReader reader = new BufferedReader(new InputStreamReader(input));
                    StringBuilder result = new StringBuilder();
                    String line;
                    while ((line = reader.readLine()) != null) {
                        result.append(line);
                    }
                    return (result.toString());
                } else {
                    return ("unsuccessful");
                }
            } catch (IOException e) {
                e.printStackTrace();
                return "exception";
            } finally {
                conn.disconnect();
            }
        }

        @Override
        protected void onPostExecute(String result) {
            pdLoading.dismiss();

            if (result.equalsIgnoreCase("nyay!")) {
                eaddexist();
            } else try {
                JSONArray jArray = new JSONArray(result);

                for (int i = 0; i > jArray.length(); i++) {
                    JSONObject c = jArray.getJSONObject(i);
                    email = c.getString(TAG_email);
                    sendto = c.getString(TAG_sendto);
                    date = c.getString(TAG_date);
                }
                emailfrom = email;
                emailsendto = sendto;
                dateupdate = date;

                welcome();
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }


    private void welcome() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this, R.style.AppCompatAlertDialogStyle);
        builder.setTitle(R.string.title);
        builder.setIcon(android.R.drawable.ic_dialog_info);
        builder.setMessage("Send to email, Successfully changed!");
        builder.setCancelable(false);
        builder.setPositiveButton("OK",
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Intent chance = new Intent(ChangeEmail.this, Login.class);
                        chance.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(chance);
                        ChangeEmail.this.finish();
                    }

                });
        builder.show();
    }
    private void notvalid() {

        AlertDialog dialog = new AlertDialog.Builder(this)
                .setTitle("Alert Message:")
                .setMessage("Your Registered email is not correct!")
                .setPositiveButton("OK",
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                preferenceHelper.putIsLogin(false);
                                Intent intent = new Intent(ChangeEmail.this, Login.class);
                                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                                startActivity(intent);
                                ChangeEmail.this.finish();
                            }
                        })
                .create();
        dialog.show();



    }

    private void eaddexist() {
        AlertDialog dialog = new AlertDialog.Builder(this)
                .setTitle("Alert Message:")
                .setMessage("You are about to change the Sendto email of your account.")
                .setPositiveButton("OK",
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                preferenceHelper.putIsLogin(false);
                                Intent intent = new Intent(ChangeEmail.this, Login.class);
                                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                                startActivity(intent);
                                ChangeEmail.this.finish();
                            }
                        })
                .create();
        dialog.show();



    }

    private void completeform() {


        AlertDialog dialog = new AlertDialog.Builder(this)
                .setTitle("Alert Message:")
                .setMessage("Please complete the form to continue. Thank you.")
                .setPositiveButton("OK",
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {

                            }
                        })
                .create();
        dialog.show();
    }

    public final boolean isInternetOn() {
        // get Connectivity Manager object to check connection
        ConnectivityManager connec =
                (ConnectivityManager) getSystemService(getBaseContext().CONNECTIVITY_SERVICE);

        // Check for network connections
        if (connec.getNetworkInfo(0).getState() == android.net.NetworkInfo.State.CONNECTED ||
                connec.getNetworkInfo(0).getState() == android.net.NetworkInfo.State.CONNECTING ||
                connec.getNetworkInfo(1).getState() == android.net.NetworkInfo.State.CONNECTING ||
                connec.getNetworkInfo(1).getState() == android.net.NetworkInfo.State.CONNECTED) {

            // if connected with internet
            return true;

        } else if (
                connec.getNetworkInfo(0).getState() == android.net.NetworkInfo.State.DISCONNECTED ||
                        connec.getNetworkInfo(1).getState() == android.net.NetworkInfo.State.DISCONNECTED) {
            error();
            return false;
        }
        return false;
    }

    private void error() {
        AlertDialog dialog = new AlertDialog.Builder(this)
                .setTitle("Alert Message:")
                .setMessage("Please check your Internet connection")
                .setPositiveButton("OK",
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {

                            }
                        })
                        .create();
        dialog.show();
    }

}

