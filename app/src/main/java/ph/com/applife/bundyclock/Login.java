package ph.com.applife.bundyclock;

import android.content.Intent;
import android.os.AsyncTask;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdSize;
import com.google.android.gms.ads.AdView;

import com.google.android.gms.ads.MobileAds;

import org.json.JSONException;

import java.io.IOException;
import java.util.HashMap;

public class Login extends AppCompatActivity {

  private AdView mAdView;

  private EditText etusername, etpassword;
  private Button btnlogin, btncancel;
  private TextView tvreg;
  private TextView forgots;
  private ParseContent parseContent;
  private final int LoginTask = 1;
  private PreferenceHelper preferenceHelper;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_login);

    preferenceHelper = new PreferenceHelper(this);
    parseContent = new ParseContent(this);

    if(preferenceHelper.getIsLogin()){
      Intent intent = new Intent(Login.this,Dashboard.class);
      intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
      startActivity(intent);
      this.finish();
    }




    mAdView = findViewById(R.id.adView);
    AdRequest adRequest = new AdRequest.Builder().build();
    mAdView.loadAd(adRequest);


    parseContent = new ParseContent(this);
    preferenceHelper = new PreferenceHelper(this);

    etusername = (EditText) findViewById(R.id.txtuser);
    etpassword = (EditText) findViewById(R.id.txtpass);

    btnlogin = (Button) findViewById(R.id.btnLogin);
    btncancel=(Button)findViewById(R.id.btnCancel);
    forgots=(TextView) findViewById(R.id.forge);
    forgots.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        Intent forgot = new Intent(Login.this, ForgotPassword.class);
        startActivity(forgot);
        Login.this.finish();
      }
    });
    tvreg = (TextView) findViewById(R.id.acct);
    tvreg.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        Intent intent = new Intent(Login.this, Register.class);
        startActivity(intent);

        Login.this.finish();
      }
    });

    btncancel.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        Intent dash=new Intent(Login.this, MainActivity.class);
        startActivity(dash);
        Login.this.finish();
      }
    });
    btnlogin.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        try {
          login();
        } catch (IOException e) {
          e.printStackTrace();
        } catch (JSONException e) {
          e.printStackTrace();
        }
      }
    });
  }
  private void login() throws IOException, JSONException {

    if (!ApplifeUtils.isNetworkAvailable(Login.this)) {
      Toast.makeText(Login.this, "Internet is required!", Toast.LENGTH_SHORT).show();
      return;
    }
    ApplifeUtils.showSimpleProgressDialog(Login.this);
    final HashMap<String, String> map = new HashMap<>();
    map.put(ApplifeConstants.Params.USERNAME, etusername.getText().toString());
    map.put(ApplifeConstants.Params.PASSWORD, etpassword.getText().toString());
    new AsyncTask<Void, Void, String>(){
      protected String doInBackground(Void[] params) {
        String response="";
        try {
          HttpRequest req = new HttpRequest(ApplifeConstants.ServiceType.LOGIN);
          response = req.prepare(HttpRequest.Method.POST).withData(map).sendAndReadString();
        } catch (Exception e) {
          response=e.getMessage();
        }
        return response;
      }
      protected void onPostExecute(String result) {
        //do something with response
        Log.d("newwwss", result);
        onTaskCompleted(result,LoginTask);
      }
    }.execute();
  }

  private void onTaskCompleted(String response,int task) {
    Log.d("responsejson", response.toString());
    ApplifeUtils.removeSimpleProgressDialog();  //will remove progress dialog
    switch (task) {
      case LoginTask:
        if (parseContent.isSuccess(response)) {
          parseContent.saveInfo(response);
          Toast.makeText(Login.this, "Login Successfully!", Toast.LENGTH_SHORT).show();
          Intent intent = new Intent(Login.this,Dashboard.class);
          intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
          startActivity(intent);
          this.finish();
        }else {
          Toast.makeText(Login.this, parseContent.getErrorMessage(response), Toast.LENGTH_SHORT).show();
        }
    }
  }
}
