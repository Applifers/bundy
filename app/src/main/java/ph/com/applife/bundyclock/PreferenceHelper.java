package ph.com.applife.bundyclock;

import android.content.Context;
import android.content.SharedPreferences;

public class PreferenceHelper {

    private final String INTRO = "intro";
    private final String NAME = "name";
    private final String SENDTO = "sendto";
    private final String EMAIL = "email";
    private final String EMPNUM="empnum";
    private final String REPORTID="reportID";
    private final String time = "time" ;
    private final String timeout = "timeout" ;
    private SharedPreferences app_prefs;

    private Context context;

    public PreferenceHelper(Context context) {
        app_prefs = context.getSharedPreferences("shared",
                Context.MODE_PRIVATE);
        this.context = context;
    }

    public void putIsLogin(boolean loginorout) {
        SharedPreferences.Editor edit = app_prefs.edit();
        edit.putBoolean(INTRO, loginorout);
        edit.commit();
    }
    public boolean getIsLogin() {
        return app_prefs.getBoolean(INTRO, false);
    }

    public void setdisable (boolean session) {
        SharedPreferences.Editor edit = app_prefs.edit();
        edit.putBoolean("session", session);
        edit.commit();
    }
    public boolean getdisable() {
        return app_prefs.getBoolean("session", false);
    }

    public void putName(String loginorout) {
        SharedPreferences.Editor edit = app_prefs.edit();
        edit.putString(NAME, loginorout);
        edit.commit();
    }
    public String getName() {
        return app_prefs.getString(NAME, "");
    }
    public void setTime(String loginorout) {
        SharedPreferences.Editor edit = app_prefs.edit();
        edit.putString(time, loginorout);
        edit.commit();
    }
    public String getTime() {
        return app_prefs.getString(time, "");
    }
    public void setTimeout(String loginorout) {
        SharedPreferences.Editor edit = app_prefs.edit();
        edit.putString(timeout, loginorout);
        edit.commit();
    }
    public String getTimeout() {
        return app_prefs.getString(timeout, "");
    }

    public String getSENDTO() {
        return app_prefs.getString(SENDTO, "");
    }
    public void putHobby(String loginorout) {
        SharedPreferences.Editor edit = app_prefs.edit();
        edit.putString(SENDTO, loginorout);
        edit.commit();
    }
    public String getHobby(){return app_prefs.getString(SENDTO,"");
    }

    public void putEm(String loginorout) {
        SharedPreferences.Editor edit = app_prefs.edit();
        edit.putString(EMAIL, loginorout);
        edit.commit();
    }
    public String getEm(){return app_prefs.getString(EMAIL,"");
    }

    public void putNum(String loginorout) {
        SharedPreferences.Editor edit = app_prefs.edit();
        edit.putString(EMPNUM, loginorout);
        edit.commit();
    }
    public String getNum(){return app_prefs.getString(EMPNUM,"");
    }
    public void putReportID(String loginorout) {
        SharedPreferences.Editor edit = app_prefs.edit();
        edit.putString(REPORTID, loginorout);
        edit.commit();
    }
    public String getReportID(){return app_prefs.getString(REPORTID,"");
    }

}
