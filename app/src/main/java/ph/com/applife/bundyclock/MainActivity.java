package ph.com.applife.bundyclock;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdSize;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.MobileAds;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.remoteconfig.FirebaseRemoteConfig;
import com.google.firebase.remoteconfig.FirebaseRemoteConfigSettings;

import java.util.HashMap;

public class MainActivity extends AppCompatActivity {
    private Button btnlogin;
    private Button btnregister;

    private ParseContent parseContent;
    private PreferenceHelper preferenceHelper;
    private FirebaseRemoteConfig firebaseRemoteConfig = FirebaseRemoteConfig.getInstance();
    private HashMap<String,Object> firebaseDefault;
    private final String LATEST_APP_VERSION_KEY = "latest_app_version";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        preferenceHelper = new PreferenceHelper(this);
        parseContent = new ParseContent(this);
        firebaseDefault = new HashMap<>();
        firebaseDefault.put(LATEST_APP_VERSION_KEY,getCurrentVersionCode());
        firebaseRemoteConfig.setDefaults(firebaseDefault);
        firebaseRemoteConfig.setConfigSettings(new FirebaseRemoteConfigSettings.Builder().setDeveloperModeEnabled(true).build());
        firebaseRemoteConfig.fetch().addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {

                if (task.isSuccessful()){
                    firebaseRemoteConfig.activateFetched();
                    checkForUpdate();
                }
                else {
                    Toast.makeText(MainActivity.this, "Something went wrong fetching", Toast.LENGTH_SHORT).show();
                }

            }
        });
        if(preferenceHelper.getIsLogin()){
            Intent intent = new Intent(MainActivity.this,Dashboard.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
            this.finish();
        }

        btnlogin=(Button)findViewById(R.id.btnSignin);
        btnregister=(Button)findViewById(R.id.btnRegister);

        btnlogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent log=new Intent(MainActivity.this, Login.class);
                startActivity(log);
                MainActivity.this.finish();
            }
        });

        btnregister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent reg=new Intent(MainActivity.this, Register.class);
                startActivity(reg);
                MainActivity.this.finish();
            }
        });

    }

    private void checkForUpdate() {
        int fetchedVersionCode = (int) firebaseRemoteConfig.getDouble(LATEST_APP_VERSION_KEY);
        if(getCurrentVersionCode() < fetchedVersionCode) {


            AlertDialog dialog = new AlertDialog.Builder(this)
                    .setTitle("New version available")
                    .setMessage("Please, update app to new version")
                    .setPositiveButton("Update",
                            new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    redirectStore("https://play.google.com/store/apps/details?id=ph.com.applife.bundyclock");
                                }
                            }).setNegativeButton("Later",
                            new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {

                                }
                            }).create();
            dialog.show();


        }
        else {
            Toast.makeText(this, "your app is already updated", Toast.LENGTH_SHORT).show();
        }
    }

    private void redirectStore(String updateUrl) {
        final Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(updateUrl));
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
    }
    private int getCurrentVersionCode() {
        try {
            return getPackageManager().getPackageInfo(getPackageName(),0).versionCode;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        return  -1;
    }
}
